﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Servicio_WCF_ClinicaOdontologica
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        private double baseExp;

        public string sumar(int value1, int value2)
        {
            return (value1 + value2).ToString();

        }
        public int multiplicacion(int valor1, int valor2)
        {
            return (valor1 * valor2);
        }

        public int restar(int valor1, int valor2)
        {
            return (valor1 - valor2);
        }

        public double division(double valor1, double valor2)
        {
            return (valor1 / valor2);
        }

        public double potencia(double baseExp, double exponente)
        {
            return Math.Pow(baseExp, exponente);
        }

        public double raiz(int valor)
        {
            return Math.Sqrt(valor);
        }

    }
}
