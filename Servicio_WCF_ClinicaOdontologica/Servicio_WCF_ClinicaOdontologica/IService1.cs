﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Servicio_WCF_ClinicaOdontologica
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {

        [OperationContract]
        string sumar(int value1, int value2);
        [OperationContract]
        int restar(int valor1, int valor2);
        [OperationContract]
        int multiplicacion(int valor1, int valor2);
        [OperationContract]
        double division(double valor1, double valor2);
        [OperationContract]
        double potencia(double baseExp, double exponentes);
        [OperationContract]
        double raiz(int valor);


    }
}
